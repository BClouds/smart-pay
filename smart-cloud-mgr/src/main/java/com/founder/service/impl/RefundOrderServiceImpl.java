package com.founder.service.impl;

import com.founder.core.dao.RefundOrderRespository;
import com.founder.core.domain.RefundOrder;
import com.founder.core.log.MyLog;
import com.founder.service.IRefundOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Service
public class RefundOrderServiceImpl implements IRefundOrderService {

    private static final MyLog _log = MyLog.getLog(RefundOrderServiceImpl.class);

    @Autowired
    RefundOrderRespository refundOrderRespository;

    @Override
    public RefundOrder selectRefundOrder(String refundOrderId) {
        return refundOrderRespository.getOne(refundOrderId);
    }

    @Override
    public Page<RefundOrder> selectRefundOrderList(int offset, int limit, RefundOrder refundOrder) {
        _log.info("分页查询退单，offset={}，limit={}。", offset, limit);
        Pageable pageable = PageRequest.of(offset, limit);
        Page<RefundOrder> page = refundOrderRespository.findAll(new Specification<RefundOrder>() {
            @Override
            public Predicate toPredicate(Root<RefundOrder> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate = criteriaBuilder.conjunction();
                if (refundOrder != null){
                    String mchId = refundOrder.getMchId();
                    if (StringUtils.isBlank(mchId)){
                        _log.info("传入商户号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"));
                    } else {
                        _log.info("退款订单按商户号模糊查询");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("mchId"), "%"+refundOrder.getMchId().trim()+"%"));
                    }
                    String refundOrderId = refundOrder.getRefundOrderId();
                    if (StringUtils.isBlank(refundOrderId)){
                        _log.info("传入退款单号为空标识查询全部");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("refundOrderId"), "%"));
                    } else {
                        _log.info("退款单号模糊查询");
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("refundOrderId"), "%"+refundOrder.getRefundOrderId().trim()+"%"));
                    }
                    Integer status = refundOrder.getStatus();
                    if (status != null && status != -99){
                        _log.info("订单状态-99标识全部");
                        predicate.getExpressions().add(criteriaBuilder.equal(root.get("status"), refundOrder.getStatus()));
                    }
                }

                _log.debug("构造查询条件");
                criteriaQuery.where(predicate);
                _log.debug("按照退款订单创建时间排序");
                criteriaQuery.orderBy(criteriaBuilder.desc(root.get("createTime").as(Date.class)));

                return criteriaQuery.getRestriction();
            }
        }, pageable);
        return page;
    }

    @Override
    public List<RefundOrder> getRefundOrderList(int offset, int limit, RefundOrder refundOrder) {
        Pageable pageable = PageRequest.of(offset,limit);
        Example<RefundOrder> example = Example.of(refundOrder);
        Page<RefundOrder> page = refundOrderRespository.findAll(example,pageable);
        return page.getContent();
    }

    @Override
    public Integer count(RefundOrder refundOrder) {
        Example<RefundOrder> example = Example.of(refundOrder);
        Long count = refundOrderRespository.count(example);
        return count.intValue();
    }
}
