package com.founder.service;

import com.founder.core.domain.MchNotify;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IMchNotifyService {

    MchNotify selectMchNotify(String orderId);

    Page<MchNotify> selectMchNotifyList(int offset, int limit, MchNotify mchNotify);

    @Deprecated
    List<MchNotify> getMchNotifyList(int offset, int limit, MchNotify mchNotify);

    @Deprecated
    Integer count(MchNotify mchNotify);
}
