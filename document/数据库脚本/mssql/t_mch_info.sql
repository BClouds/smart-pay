
CREATE TABLE [dbo].[t_mch_info](
	[MchId] [varchar](30) NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[Type] [varchar](24) NOT NULL,
	[ReqKey] [varchar](128) NOT NULL,
	[ResKey] [varchar](128) NOT NULL,
	[State] [int] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_mch_info] ADD  DEFAULT ('1') FOR [State]
GO

ALTER TABLE [dbo].[t_mch_info] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[t_mch_info] ADD  DEFAULT (getdate()) FOR [UpdateTime]
GO


