package com.founder.core.dao;

import com.founder.core.domain.FianceWx;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public interface FianceWxRespository extends JpaRepository<FianceWx, Integer> {

    @Modifying
    @Query(value = "delete from FianceWx where mchid = :mchId and date = :date")
    int deleteAllByMchidAndDate(@Param(value = "mchId") String mchId, @Param(value = "date") String date);

    @Query(value = "from FianceWx where mchid = :mchId and date = :date and bzorder like %:keyName% ")
    List<FianceWx> queryAllByMchidAndBzorder(@Param(value = "mchId") String mchId, @Param(value = "date") String date, @Param(value = "keyName") String keyName);

    @Query(value = "from FianceWx where mchid = :mchId and date = :date and bzorder not like %:keyName% ")
    List<FianceWx> queryAllByMchidAndBzorderNot(@Param(value = "mchId") String mchId, @Param(value = "date") String date, @Param(value = "keyName") String keyName);
}
