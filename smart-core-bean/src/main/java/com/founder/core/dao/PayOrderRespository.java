package com.founder.core.dao;

import com.founder.core.domain.PayOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
@Repository
public interface PayOrderRespository extends JpaRepository<PayOrder, String>, JpaSpecificationExecutor<PayOrder> {

    @Transactional
    @Modifying
    @Query(value = "update PayOrder set status = 1 where mchId = :mchId and payOrderId = :payOrderId")
    int jpa_updateStatus4Ing(@Param("mchId") String mchId, @Param("payOrderId") String payOrderId);
}
